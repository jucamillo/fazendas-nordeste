<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package fazendas_nordeste
 */

get_header();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>



	<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/gsap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js"></script>

	
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>

<?php while ( have_posts() ) : the_post(); ?>
<?php 
$main_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
$images = get_field('galeria'); ?>


<section class="galeria-single desktop-gal">
	<?php if( $images ): ?>
	<div class="main-image">
		<a href="<?php echo $main_image[0]; ?>"  data-fancybox="gallery" class="img" style="background-image:url(<?php echo $main_image[0]; ?>);" data-caption="<?php echo the_title(); ?>">
			<div class="btn btn-all">
				<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M19 3H5C3.89543 3 3 3.89543 3 5V19C3 20.1046 3.89543 21 5 21H19C20.1046 21 21 20.1046 21 19V5C21 3.89543 20.1046 3 19 3Z" stroke="#FDFFF7" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M8.5 10C9.32843 10 10 9.32843 10 8.5C10 7.67157 9.32843 7 8.5 7C7.67157 7 7 7.67157 7 8.5C7 9.32843 7.67157 10 8.5 10Z" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
					<path d="M21 15L16 10L5 21" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
				</svg>
				<span>
					Mostrar todas as fotos
				</span>
			</div>


		</a>
	</div>
    <ul>
        <?php foreach( $images as $image ): ?>
            <li>
                <a href="<?php echo esc_url($image['url']); ?>"  data-fancybox="gallery" class="img" style="background-image:url(<?php echo esc_url($image['url']); ?>);" data-caption="<?php echo esc_html($image['caption']); ?>">

                </a>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php else: ?>
	<div class="main-image full">
		<a href="<?php echo $main_image[0]; ?>"  data-fancybox="gallery" class="img" style="background-image:url(<?php echo $main_image[0]; ?>);" data-caption="<?php echo the_title(); ?>">
		</a>
	</div>
    <?php endif; ?>
</section>


<section class="galeria-single mobile-gal">
    <ul class="owl-carousel owl-galeria">
    	<li>
			<a href="<?php echo $main_image[0]; ?>"  data-fancybox="gallery" class="img" style="background-image:url(<?php echo $main_image[0]; ?>);" data-caption="<?php echo the_title(); ?>">
			</a>
    	</li>
    	<?php if( $images ): ?>
	        <?php foreach( $images as $image ): ?>
	            <li>
	                <a href="<?php echo esc_url($image['url']); ?>"  data-fancybox="gallery" class="img" style="background-image:url(<?php echo esc_url($image['url']); ?>);" data-caption="<?php echo esc_html($image['caption']); ?>">

	                </a>
	            </li>
	        <?php endforeach; ?>
	    <?php endif; ?>
    </ul>
</section>


<section class="single-info-imovel">
	<div class="container">
		<article class="col-xs-12 col-sm-6 col-md-7">
			<h5 class="tipo"><span>
				<?php
					$terms = get_the_terms( $post->ID , 'tipo' );
		            foreach ( $terms as $term ) {
		                echo  ''.$term->name.'';
		            }
	            ?></span>
	        </h5>

	        <h1>
	        	<?php the_title(); ?> <?php echo get_field('cod'); ?>
	        </h1>
	        <?php 
			if( get_field('valor') ):
			$valor = get_field('valor'); 
			$valor_Mil = number_format($valor,2,',','.');
			?>
		        <h4 class="valor">
		        	R$ <?php echo $valor_Mil; ?>
		        </h4>
			<?php endif; ?>

			<h5 class="carac">
				Características
			</h5>
			<?php 
			echo '<ul class="infos">';
	            $estados = get_the_terms( $post->ID , 'estado' );
	            if($estados){
	                echo  '<li>
	                    <img src="'.get_template_directory_uri().'/images/estado.svg">
	                    <span> <small>Estado</small>';
	                foreach ( $estados as $estado ) {
	                    echo  '<strong>'.$estado->name.'</strong>';
	                }

	                echo  '</span></li>';
	            }

	            /*$regiaos = get_the_terms( $post->ID , 'regiao' );
	            if($regiaos){
	                echo  '<li>
	                    <img src="'.get_template_directory_uri().'/images/regiao.svg">
	                    <span> <small>Região</small>';
	                foreach ( $regiaos as $regiao ) {
	                    echo  '<strong>'.$regiao->name.'</strong>';
	                }

	                echo  '</span></li>';
	            }*/


                $regiao_post = get_field('regiao_post');
                if( $regiao_post ): 
                    $string .= '<li>
                        <img src="'.get_template_directory_uri().'/images/regiao.svg">
                        <span> <small>Região</small>';
                        $string .= '<strong>'.esc_html( $regiao_post->post_title ).'</strong>';

                    $string .= '</span></li>';
                endif;

	            $climas = get_the_terms( $post->ID , 'clima' );
	            if($climas){
	                echo  '<li>
	                    <img src="'.get_template_directory_uri().'/images/clima.svg">
	                    <span> <small>Clima</small>';
	                foreach ( $climas as $clima ) {
	                    echo  '<strong>'.$clima->name.'</strong>';
	                }
	                echo  '</span></li>';
	            }

	            $topografias = get_the_terms( $post->ID , 'topografia' );
	            if($topografias){
	                echo  '<li>
	                    <img src="'.get_template_directory_uri().'/images/topografia.svg">
	                    <span> <small>Topografia</small>';
	                foreach ( $topografias as $topografia ) {
	                    echo  '<strong>'.$topografia->name.'</strong>';
	                }
	                echo  '</span></li>';
	            }

	            echo  '<li class="area">';
	            echo  ' <span><img src="'.get_template_directory_uri().'/images/hectar.svg"> <small>Área total</small></span><span>';
		            if( get_field('hectares') ): 
		                    echo  '<strong>'.get_field('hectares').'<br>Hectares</strong>';
		            endif;
		            if( get_field('alqueires') ): 
		                    echo  '<strong>'.get_field('alqueires').'<br>Metros</strong>';
		            endif;
		            if( get_field('tarefas') ): 
		                    echo  '<strong>'.get_field('tarefas').'<br>Tarefa</strong>';
		            endif;
	            echo  '</span></li>';
	        echo  '</ul>';

	        ?>



	        <?php if ( !empty( get_the_content() ) ): ?>
	        	<div class="info">
					<h5>
						Descrição
					</h5>
					<?php the_content(); ?>
				</div>
	        <?php endif; ?>



        	<div class="info">



	            <ul class="infos">

			        <?php  if( get_field('plantacoes') ): ?>
			        	<li class="area">
			        		<span>
			        			<img src="<?php echo get_template_directory_uri(); ?>/images/plantacoes.svg">
								<small>Plantações</small>
							</span>
							<div>
								<?php echo get_field('plantacoes'); ?>
							</div>
						</li>
			        <?php endif; ?>


		            <?php $hidricos = get_the_terms( $post->ID , 'hidrico' );
		            if($hidricos){ 
		                echo  '<li>
		                    <img src="'.get_template_directory_uri().'/images/hidrico.svg">
		                    <span> <small>Recursos Hídricos</small><strong class="hid">';
		                foreach ( $hidricos as $hidrico ) {
		                    echo  '<span>'.$hidrico->name.'</span>';
		                }
		                echo  '</strong></span></li>';
		            }  ?>
			        <?php  if( get_field('periodo_chuvoso') ): 
		                echo  '<li> <img src="'.get_template_directory_uri().'/images/chuva.svg">
		                    <span> <small>Período Chuvoso</small><strong>';
		                   echo get_field('periodo_chuvoso');
		                echo  '</strong></span></li>';
		                ?>
			        <?php endif; ?>
	            </ul>
			</div>


		    <?php  if( get_field('opiniao') ): ?>
	        	<div class="info">
					<h5>
						Opnião do Corretor
					</h5>
					<?php echo get_field('opiniao'); ?>
				</div>
	        <?php endif; ?>

	        <?php
			$featured_post = get_field('regiao_post');

			if( $featured_post ): 
				$permalink = get_permalink( $featured_post->ID ); 
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $featured_post->ID ), 'single-post-thumbnail' );
				?>
	        	<div class="info">
					<h5>
						Região
					</h5>
					<div class="reg">
						<div class="img">
							<a href="<?php echo $permalink; ?>" title="<?php echo esc_html( $featured_post->post_title ); ?>" style="background-image: url(<?php echo $image[0]; ?>);">
								
							</a>
						</div>
						<div class="txt">
							<h5><?php echo esc_html( $featured_post->post_title ); ?></h5>
							<p>
								<?php echo esc_html( $featured_post->post_excerpt ); ?>
							</p>
							<a href="<?php echo $permalink; ?>" title="<?php echo esc_html( $featured_post->post_title ); ?>" class="more">
								Saiba mais sobre a região
							</a>
						</div>
					</div>
				</div>
			<?php endif; ?>







				<?php if( get_field('aviso', 'option') ): ?>
		        	<div class="info aviso">
						<?php the_field('aviso', 'option'); ?>
					</div>
				<?php endif; ?>

		</article>
		<aside class="col-xs-12 col-sm-6 col-md-5">
			<div id="trigger1"></div>
			<div class="formulario" id="pin1">
				<?php the_field('chamada'); ?>

			</div>
		</aside>
	</div>
</section>
<section class="relacionados_imv">
	<div class="container">
		<div class="col-xs-12">
			<?php the_field('imv_rl', 'option'); ?>
			<?php 
			$tipos = get_the_terms( $post->ID , 'tipo' );
            foreach ( $tipos as $tipo ) {
			    $bannerArgs = array( 
			    	'post_type' => 'imovel',
			        'posts_per_page' => 3, 
			        'orderby'=>'rand',
			        'order'=>'rand',
			        'post__not_in' => array($post->ID),
					 'tax_query' => array(
					  array(
					   'taxonomy' => 'tipo',
					   'field' => 'slug',
					   'terms' => $tipo->slug
					  )
					 )
			    );
			}
		    

		    $bannerLoop = new WP_Query( $bannerArgs ); 
		    echo  '<ul class="imoveis-list">';
		    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
		                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		                echo  '<li>';
		                    $terms = get_the_terms( $post->ID , 'tipo' );
		                    echo  '<a href="'.get_the_permalink().'" title="'.get_the_title().'"><div  class="img"><div  class="im" style="background-image:url('.$image[0].');" ></div>';
		                        foreach ( $terms as $term ) {
		                            echo  '<span class="tipo">'.$term->name.'</span>';
		                        }
		                        echo  '</div>
		                        <div class="info">
		                            <h4>'.get_the_title().'</h4>
		                            <ul class="infos">';
		                                $estados = get_the_terms( $post->ID , 'estado' );
		                                if($estados){
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/estado.svg">
		                                        <span> <small>Estado</small>';
		                                    foreach ( $estados as $estado ) {
		                                        echo  '<strong>'.$estado->name.'</strong>';
		                                    }

		                                    echo  '</span></li>';
		                                }
		                                /*
		                                $regiaos = get_the_terms( $post->ID , 'regiao' );
		                                if($regiaos){
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/regiao.svg">
		                                        <span> <small>Região</small>';
		                                    foreach ( $regiaos as $regiao ) {
		                                        echo  '<strong>'.$regiao->name.'</strong>';
		                                    }

		                                    echo  '</span></li>';
		                                }*/


	                                $regiao_post = get_field('regiao_post');
	                                if( $regiao_post ): 
	                                    $string .= '<li>
	                                        <img src="'.get_template_directory_uri().'/images/regiao.svg">
	                                        <span> <small>Região</small>';
	                                        $string .= '<strong>'.esc_html( $regiao_post->post_title ).'</strong>';

	                                    $string .= '</span></li>';
	                                endif;

		                                $climas = get_the_terms( $post->ID , 'clima' );
		                                if($climas){
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/clima.svg">
		                                        <span> <small>Clima</small>';
		                                    foreach ( $climas as $clima ) {
		                                        echo  '<strong>'.$clima->name.'</strong>';
		                                    }
		                                    echo  '</span></li>';
		                                }

		                                $topografias = get_the_terms( $post->ID , 'topografia' );
		                                if($topografias){
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/topografia.svg">
		                                        <span> <small>Topografia</small>';
		                                    foreach ( $topografias as $topografia ) {
		                                        echo  '<strong>'.$topografia->name.'</strong>';
		                                    }
		                                    echo  '</span></li>';
		                                }

		                                if( get_field('hectares') ): 
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/hectar.svg">
		                                        <span> <small>Área total</small>';
		                                        echo  '<strong>'.get_field('hectares').' Hectares</strong>';
		                                    echo  '</span></li>';
		                                endif;
		                            echo  '</ul>
		                            </div>
		                        </a>
		                    </li>';
		    endwhile;
		    echo  '</ul>';
		    ?>
		</div>
	</div>
</section>


<section class="fix-btn">
	<div class="container">
		<div class="col-xs-12">
			<div class="wpcf7">
				<button class="wpcf7-submit">
					Agendar Visita
				</button>
			</div>
		</div>
	</div>
</section>
	
<?php endwhile; ?>

<script type="text/javascript">
if( jQuery(window).width() > 767 ){
	var controller = new ScrollMagic.Controller();
	jQuery(function(){
	
		var scene = new ScrollMagic.Scene({triggerElement: "#trigger1", triggerHook: "onLeave", duration: (jQuery(".single-info-imovel article").height() - jQuery(".single-info-imovel aside .formulario").height() + 70 ) })
		.setPin("#pin1")
		.addTo(controller);

	});
};




            var maskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            options = {onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
                }
            };

            jQuery('input[name="whatsapp"]').mask(maskBehavior, options);

</script>
<?php
get_footer();
