<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fazendas_nordeste
 */

?>
	</section>

	<footer id="colophon" class="site-footer">


        <?php if( have_rows('cotacoes', 'option') ): ?>
            <section class="cotacao">
            <div class="container">
                <div class="col-md-2 col-xs-12 titulo">
                    <h4>
                        Estados<br>
                        <small>
                            Nordeste
                        </small>
                    </h4>
                </div>
                <div class="col-md-10 col-xs-12">
                    <ul class="estados estados-owl">
                        <?php $wcatTerms = get_terms('estado', array('hide_empty' => 0, 'parent' =>0)); 
                        foreach($wcatTerms as $wcatTerm) : 
                            //$thumb_id = get_woocommerce_term_meta( $wcatTerm->term_id, 'thumbnail_id', true );
                            //$term_img = wp_get_attachment_url(  $thumb_id );
                            $bandeira = get_field('bandeira', $wcatTerm);

                            ?>
                                <li class="estado-<?php echo $wcatTerm->slug; ?>">
                                    <a href="<?php echo get_home_url(); ?>/imoveis/?_sft_estado=<?php echo $wcatTerm->slug; ?>">
                                        <span><?php echo $wcatTerm->name; ?></span>
                                        <img src="<?php echo $bandeira ?>">
                                    </a>
                                </li>

                        <?php  endforeach; ?>
                    </ul>
                </div>
            </div>
        </section>
        <?php endif; ?>
        <section class="rodape">
            <div class="container">
                <div class="col-md-4 col-xs-12">
                    <?php the_custom_logo(); ?>
                    <?php if( get_field('descricao', 'option') ): ?>
                        <p>
                            <?php the_field('descricao', 'option'); ?>
                        </p>
                    <?php endif; ?> 

                    
                </div>
                <div class="col-md-1 col-xs-12">
                </div>
                <div class="col-md-4 col-xs-12 contato-miolo">
                    <?php the_field('contato', 'option'); ?>


                    <?php if( get_field('link_whatsapp', 'option') ): ?>
                        <a href="<?php the_field('link_whatsapp', 'option'); ?>" target="_blank" title="<?php the_field('chamada_whats', 'option'); ?>" class="btn">
                            <i class="fab fa-whatsapp"></i>
                            <span>
                            <?php if( get_field('chamada_whats', 'option') ): ?>
                                <?php the_field('chamada_whats', 'option'); ?>
                            <?php else: ?>
                                Fale no Whatsapp
                            <?php endif; ?> 
                            </span>                         
                        </a>
                    <?php endif; ?>
                </div>
                <div class="col-md-3 col-xs-12">

                    <?php if( have_rows('redes_sociais', 'option') ): ?>
                        <ul class="social">
                        <?php while( have_rows('redes_sociais', 'option') ): the_row();
                            ?>
                            <li>
                                <a href="<?php the_sub_field('link'); ?>" target="_blank">
                                    <?php the_sub_field('icone'); ?>
                                </a>
                            </li>
                        <?php endwhile; ?>
                    </ul>
                    <?php endif; ?>


                    <?php
                    wp_nav_menu( array(
                        'theme_location' => 'menu-1',
                    ) );
                    ?>

                </div>
            </div>
            

        </section>

	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

<script type="text/javascript">
	<?php if ( ! is_admin() ) { ?>

            jQuery(function(){


                var hasBeenTrigged = false;
                jQuery(window).scroll(function() {
                    if (jQuery(this).scrollTop() >= 50 && !hasBeenTrigged) { // if scroll is greater/equal then 100 and hasBeenTrigged is set to false.
                         jQuery('header').addClass('scroll');
                        hasBeenTrigged = true;
                    } else if(jQuery(this).scrollTop() < 50 && hasBeenTrigged){
                         jQuery('header').removeClass('scroll');
                        hasBeenTrigged = false;

                    }
                });




                jQuery('.sf-input-select').select2();
                jQuery('.wpuf-fields select').select2();



                jQuery( ".wpcf7-acceptance input" ).prop( "checked", true );
                jQuery( ".single-info-imovel .formulario .wpcf7 .wpcf7-submit" ).removeAttr("disabled");





                if( jQuery(window).width() < 992 ){
                    jQuery('.whatsbtn').insertAfter('header nav.main-navigation ul li#menu-item-23');
                    jQuery('.relacionados_imv .imoveis-list').addClass('owl-carousel');

                    jQuery('.relacionados_imv .imoveis-list').owlCarousel({
                        margin:30,
                        responsiveClass:true,
                        dots: true,
                        nav:false,
                        autoHeight:true,
                        autoplay: false,
                        autoplayTimeout: 10000,
                        dotsEach: 1,
                        responsive:{
                            0:{
                                items:1,
                                margin:10,
                            },
                            500:{
                                items:1,
                            },
                            768:{
                                items:2
                            },
                            992:{
                                items:2
                            }
                        }
                    })
                } 

                if( jQuery(window).width() < 768 ){
                    jQuery(document).delegate('.fix-btn button', 'click', function(event) {
                        event.preventDefault();
                        jQuery('.single-info-imovel aside').toggleClass('active');
                    });

                    jQuery('.single-info-imovel .formulario').prepend('<button class="close-btn"></button>');

                    jQuery(document).delegate('.single-info-imovel .formulario button.close-btn', 'click', function(event) {
                        event.preventDefault();
                        jQuery('.single-info-imovel aside').toggleClass('active');
                    });
                    jQuery('ul.owl-galeria').owlCarousel({
                        margin:30,
                        responsiveClass:true,
                        dots: true,
                        nav:false,
                        autoHeight:true,
                        autoplay: false,
                        autoplayTimeout: 10000,
                        dotsEach: 1,
                        responsive:{
                            0:{
                                items:1
                            },
                            768:{
                                items:1
                            },
                            992:{
                                items:1
                            }
                        }
                    })

                    jQuery('.estados-owl').addClass('owl-carousel');
                    jQuery('.estados-owl').owlCarousel({
                        margin:0,
                        responsiveClass:true,
                        dots: true,
                        nav:false,
                        autoHeight:true,
                        autoplay: false,
                        autoplayTimeout: 10000,
                        dotsEach: 1,
                        responsive:{
                            0:{
                                items:2
                            },
                            370:{
                                items:3
                            },
                            768:{
                                items:4
                            },
                            992:{
                                items:5
                            }
                        }
                    })
                } 

                



            });
            jQuery(window).on('resize', function(){

                if( jQuery(window).width() < 992 ){
                    jQuery('.whatsbtn').insertAfter('header nav.main-navigation ul li#menu-item-23');
                    jQuery('.relacionados_imv .imoveis-list').addClass('owl-carousel');

                    jQuery('.relacionados_imv .imoveis-list').owlCarousel({
                        margin:30,
                        responsiveClass:true,
                        dots: true,
                        nav:false,
                        autoHeight:true,
                        autoplay: false,
                        autoplayTimeout: 10000,
                        dotsEach: 1,
                        responsive:{
                            0:{
                                items:1,
                                margin:10,
                            },
                            500:{
                                items:1,
                            },
                            768:{
                                items:2
                            },
                            992:{
                                items:2
                            }
                        }
                    })
                } 

                if( jQuery(window).width() < 768 ){
                    jQuery(document).delegate('.fix-btn button', 'click', function(event) {
                        event.preventDefault();
                        jQuery('.single-info-imovel aside').toggleClass('active');
                    });

                    jQuery('.single-info-imovel .formulario').prepend('<button class="close-btn"></button>');

                    jQuery(document).delegate('.single-info-imovel .formulario button.close-btn', 'click', function(event) {
                        event.preventDefault();
                        jQuery('.single-info-imovel aside').toggleClass('active');
                    });
                    jQuery('ul.owl-galeria').owlCarousel({
                        margin:30,
                        responsiveClass:true,
                        dots: true,
                        nav:false,
                        autoHeight:true,
                        autoplay: false,
                        autoplayTimeout: 10000,
                        dotsEach: 1,
                        responsive:{
                            0:{
                                items:1
                            },
                            768:{
                                items:1
                            },
                            992:{
                                items:1
                            }
                        }
                    })



                    jQuery('.estados-owl').addClass('owl-carousel');
                    jQuery('.estados-owl').owlCarousel({
                        margin:0,
                        responsiveClass:true,
                        dots: true,
                        nav:false,
                        autoHeight:true,
                        autoplay: false,
                        autoplayTimeout: 10000,
                        dotsEach: 1,
                        responsive:{
                            0:{
                                items:2
                            },
                            370:{
                                items:3
                            },
                            768:{
                                items:4
                            },
                            992:{
                                items:5
                            }
                        }
                    })

                } 

            });





            jQuery(document).delegate('.wpcf7 .dateInput-btn', 'click', function(event) {
                event.preventDefault();
                //jQuery(this).toggleClass('active');


                jQuery("#dateInput").click()

            });



            jQuery('.cotacao-owl').owlCarousel({
                margin:0,
                responsiveClass:true,
                dots: true,
                nav:false,
                autoHeight:true,
                autoplay: false,
                autoplayTimeout: 10000,
                dotsEach: 1,
                responsive:{
                    0:{
                        items:3
                    },
                    768:{
                        items:4
                    },
                    992:{
                        items:5
                    }
                }
            })


            setTimeout( function()  {
                jQuery('.banner-topo').addClass('active');

                jQuery('.page-anunciar-imovel').addClass('active');

                jQuery('.sec-imoveis.regiao').addClass('active');

                jQuery('.sec-imoveis.imv').addClass('active');

                jQuery('.filtro-sec.imv').addClass('active');
                
                jQuery('.page-contato').addClass('active');
                
                jQuery('.page-cadastro-obrigado').addClass('active');
                
                jQuery('.page-obrigado-contato').addClass('active');


            },500);






            var left1 = {
                delay: 100,
                distance: '100px',
                duration: 1300,
                origin: 'left',
                easing: 'ease-in-out',
            };

            var left2 = {
                delay: 400,
                distance: '100px',
                duration: 1300,
                origin: 'left',
                easing: 'ease-in-out',
            };

            var left3 = {
                delay: 700,
                distance: '100px',
                duration: 1300,
                origin: 'left',
                easing: 'ease-in-out',
            };


            var right1 = {
                delay: 100,
                distance: '100px',
                duration: 1300,
                origin: 'right',
                easing: 'ease-in-out',
            };
            var right2 = {
                delay: 400,
                distance: '100px',
                duration: 1300,
                origin: 'right',
                easing: 'ease-in-out',
            };
            var right3 = {
                delay: 700,
                distance: '100px',
                duration: 1300,
                origin: 'right',
                easing: 'ease-in-out',
            };





            var up1 = {
                delay: 100,
                distance: '100px',
                duration: 1300,
                origin: 'bottom',
                easing: 'ease-in-out',
            };

            var up2 = {
                delay: 400,
                distance: '100px',
                duration: 1300,
                origin: 'bottom',
                easing: 'ease-in-out',
            };

            var up3 = {
                delay: 700,
                distance: '100px',
                duration: 1300,
                origin: 'bottom',
                easing: 'ease-in-out',
            };




            var down = {
                delay: 100,
                distance: '100px',
                duration: 1000,
                origin: 'top',
                easing: 'ease-in-out',
            };




            var fade = {
                delay: 100,
                distance: '100px',
                duration: 1000,
                origin: 'bottom',
                easing: 'ease-in-out',
            };





            jQuery(document).ready(function(){



                ScrollReveal().reveal('.up1', up1);
                ScrollReveal().reveal('.up2', up2);
                ScrollReveal().reveal('.up3', up3);
                ScrollReveal().reveal('.left1', left1);
                ScrollReveal().reveal('.left2', left2);
                ScrollReveal().reveal('.left3', left3);
                ScrollReveal().reveal('.right1', right1);
                ScrollReveal().reveal('.right2', right2);
                ScrollReveal().reveal('.right3', right3);
                ScrollReveal().reveal('.down', down);
                ScrollReveal().reveal('.fade', fade);





            });

                        





<?php } ?>




<?php if ( is_page(16) ) { ?>
            var maskBehavior = function (val) {
              return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
            },
            options = {onKeyPress: function(val, e, field, options) {
                    field.mask(maskBehavior.apply({}, arguments), options);
                }
            };

            jQuery('input[name="whatsapp"]').mask(maskBehavior, options);


<?php } ?>

</script>
</body>
</html>
