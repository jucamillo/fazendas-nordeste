<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package fazendas_nordeste
 */

get_header();
?>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>



<?php while ( have_posts() ) : the_post(); ?>
<?php 
$main_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
?>


<section class="galeria-single">
	<div class="main-image full">
		<div  class="img" style="background-image:url(<?php echo $main_image[0]; ?>);">
		</div>
		<div class=" titulo-sing">
			<div class="container">
				<div class="col-xs-12">
					<h3>
						Conheça as regiões
					</h3>
					<h1>
						<?php the_title(); ?>
					</h1>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="single-info-regiao">
	<div class="container">
		<article class="col-xs-12 col-md-10 col-lg-9">
			<?php the_content(); ?>
		</article>
	</div>
</section>
<section class="relacionados_imv">
	<div class="container">
		<div class="col-xs-12">
			<?php the_field('imv_rl', 'option'); ?>
			<?php 
			$regiaos = get_the_terms( $post->ID , 'regiao' );
            foreach ( $regiaos as $regiao ) {
			    $bannerArgs = array( 
			    	'post_type' => 'imovel',
			        'posts_per_page' => 6, 
			        'orderby'=>'rand',
			        'order'=>'rand',
			        'post__not_in' => array($post->ID),
					 'tax_query' => array(
					  array(
					   'taxonomy' => 'regiao',
					   'field' => 'slug',
					   'terms' => $regiao->slug
					  )
					 )
			    );
			}
		    

		    $bannerLoop = new WP_Query( $bannerArgs ); 
		    echo  '<ul class="imoveis-list">';
		    while ( $bannerLoop->have_posts() ) : $bannerLoop->the_post();
		                $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
		                echo  '<li>';
		                    $terms = get_the_terms( $post->ID , 'tipo' );
		                    echo  '<a href="'.get_the_permalink().'" title="'.get_the_title().'"><div  class="img"><div  class="im" style="background-image:url('.$image[0].');" ></div>';
		                        foreach ( $terms as $term ) {
		                            echo  '<span class="tipo">'.$term->name.'</span>';
		                        }
		                        echo  '</div>
		                        <div class="info">
		                            <h4>'.get_the_title().'</h4>
		                            <ul class="infos">';
		                                $estados = get_the_terms( $post->ID , 'estado' );
		                                if($estados){
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/estado.svg">
		                                        <span> <small>Estado</small>';
		                                    foreach ( $estados as $estado ) {
		                                        echo  '<strong>'.$estado->name.'</strong>';
		                                    }

		                                    echo  '</span></li>';
		                                }

		                                $regiaos = get_the_terms( $post->ID , 'regiao' );
		                                if($regiaos){
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/regiao.svg">
		                                        <span> <small>Região</small>';
		                                    foreach ( $regiaos as $regiao ) {
		                                        echo  '<strong>'.$regiao->name.'</strong>';
		                                    }

		                                    echo  '</span></li>';
		                                }

		                                $climas = get_the_terms( $post->ID , 'clima' );
		                                if($climas){
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/clima.svg">
		                                        <span> <small>Clima</small>';
		                                    foreach ( $climas as $clima ) {
		                                        echo  '<strong>'.$clima->name.'</strong>';
		                                    }
		                                    echo  '</span></li>';
		                                }

		                                $topografias = get_the_terms( $post->ID , 'topografia' );
		                                if($topografias){
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/topografia.svg">
		                                        <span> <small>Topografia</small>';
		                                    foreach ( $topografias as $topografia ) {
		                                        echo  '<strong>'.$topografia->name.'</strong>';
		                                    }
		                                    echo  '</span></li>';
		                                }

		                                if( get_field('hectares') ): 
		                                    echo  '<li>
		                                        <img src="'.get_template_directory_uri().'/images/hectar.svg">
		                                        <span> <small>Área total</small>';
		                                        echo  '<strong>'.get_field('hectares').' Hectares</strong>';
		                                    echo  '</span></li>';
		                                endif;
		                            echo  '</ul>
		                            </div>
		                        </a>
		                    </li>';
		    endwhile;
		    echo  '</ul>';
		    ?>
		</div>
	</div>
</section>



	
<?php endwhile; ?>
<script type="text/javascript">
	
jQuery('.galeria').owlCarousel({
    margin:0,
    responsiveClass:true,
    dots: true,
    nav:false,
    autoHeight:true,
    autoplay: false,
    autoplayTimeout: 10000,
    dotsEach: 1,
    responsive:{
        0:{
            items:1
        },
        768:{
            items:1
        },
        1200:{
            items:1
        }
    }
})

</script>

<?php
get_footer();
