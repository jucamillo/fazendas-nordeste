<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package fazendas_nordeste
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">


    

	<?php wp_head(); ?>


	<?php if ( ! is_admin() ) { ?>
	<link rel="preconnect" href="https://fonts.gstatic.com">
	<link href="https://fonts.googleapis.com/css2?family=Arvo:ital,wght@0,700;1,400&family=Poppins:ital,wght@0,400;0,700;0,900;1,400&display=swap" rel="stylesheet">
	

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA==" crossorigin="anonymous" />
    
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css">
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>


    <script src="https://unpkg.com/scrollreveal@4"></script>


	<?php } ?>




			
		<?php if ( is_page(16) ) { ?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js" integrity="sha512-pHVGpX7F/27yZ0ISY+VVjyULApbDlD0/X0rgGbTqCE7WFW5MezNTWG/dnhtbBuICzsd0WQPgpE4REBLv+UqChw==" crossorigin="anonymous"></script>

		<?php } ?>


	<meta name="language" content="pt-BR">
	<meta name="robots" content="all">
	<meta name="author" content="Fazendas Nordeste">
	<meta name="keywords" content="fazendas, chácaras, sítios, paraíba, ceará, maranhão, piauí, rio grande do norte, pernambuco, alagoas, sergipe">
	<meta property="og:type" content="page">
	<meta property="og:url" content="fazendasnordeste.com.br">
	<meta property="og:description" content="As melhores fazendas, chácaras e sítios do Nordeste você encontra aqui. Pesquisa, tire suas dúvidas e agende uma visita agora mesmo. ">
	<meta property="article:author" content="Fazendas Nordeste">


</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Pular para o conteúdo', 'fazendas_nordeste' ); ?></a>

	<header class="site-header">
		<div class="container">
			<div class="col-xs-12">
				<div class="branding">
					<?php if ( is_front_page() ) : ?>
						<a href="<?php echo get_home_url(); ?>" title="Fazendas Nordeste" class="bco">
							<img src="<?php the_field('logo_branco', 'option'); ?>">
						</a>
					<?php else: ?>
						<?php the_custom_logo(); ?>
					<?php endif; ?>

				</div>


				<nav id="site-navigation" class="main-navigation">
					<button class="menu-toggle btn" aria-controls="primary-menu" aria-expanded="false">
						<svg width="16" height="17" viewBox="0 0 16 17" fill="none" xmlns="http://www.w3.org/2000/svg">
						<path d="M1 8.72412H15" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M1 1.96533H15" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
						<path d="M1 15.4824H15" stroke="white" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
						</svg>

						<span>
							MENU
						</span>
					</button>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
				</nav>

				<?php if( get_field('link_whatsapp', 'option') ): ?>
					<a href="<?php the_field('link_whatsapp', 'option'); ?>" target="_blank" title="<?php the_field('chamada_whats', 'option'); ?>" class="btn whatsbtn">
						<i class="fab fa-whatsapp"></i>
						<span>
						<?php if( get_field('chamada_whats', 'option') ): ?>
							<?php the_field('chamada_whats', 'option'); ?>
						<?php else: ?>
							Fale no Whatsapp
						<?php endif; ?>	
						</span>							
					</a>
				<?php endif; ?>
				
			</div>
		</div>
	</header>

	<section id="content" class="miolo-site">


	
	 <?php if ( is_front_page() ) :  
	    $gallery = get_field('imgs', 'option');
	    $rand = array_rand($gallery, 1);

	    if( $gallery ):
	    $background_image_full = wp_get_attachment_image_src( $gallery[$rand]['ID'], 'full'); ?>
	    <section class="bg-banner">
		<section class="banner-topo" style="background-image: url('<?php echo esc_url($background_image_full[0]); ?>')">
			<div class="container">
				<div class="col-md-6 col-xs-12 titulo">
					<?php the_field('titulo', 'option'); ?>
				</div>
				<div class="col-md-6 col-xs-12">
					<div class="filtro">
						<h4><?php the_field('chamada_filtro', 'option'); ?></h4>
						<?php echo do_shortcode('[searchandfilter id="45"]'); ?>
					</div>
				</div>
			</div>

			
			
		</section></section>
		<?php endif; ?>
	<?php endif; ?>
