<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package fazendas_nordeste
 */

get_header();
?>

<section class="filtro-sec imv">
	<div class="container">
		<div class="col-xs-12">
			<?php echo do_shortcode('[searchandfilter id="45"]'); ?>
		</div>
	</div>
</section>

<section class="sec-imoveis imv">
	<div class="container">
		<div class="col-xs-12">

            <?php

                global $searchandfilter;

                $sf_current_query = $searchandfilter->get(45)->current_query(); 
                if($searchandfilter){

                    echo '<h1>Resultado de busca</h1>';

                }else{
                    echo "<h1>Todos os imóveis</h1>";
                }

                /*


                if(empty($sf_current_query->get_fields_html(
                    echo "<h1>";

                        array("_sft_tipo", "_sft_estado", "_sft_regiao"), 

                            $args2

                        ))):

                        echo '<h1>';
                        
                        if(
                            $sf_current_query->get_fields_html(

                                array("_sft_estado"), 

                                $args2

                            )  == "Terreno"
                        ) {

                            echo "Terrenos";

                        } else if(
                            $sf_current_query->get_fields_html(

                                array("_sft_tipo"), 

                                $args2

                            )  == "Todos os Tipos"
                        ) {

                            echo "Todos os tipos de imóveis";

                        } else{
                            echo $sf_current_query->get_fields_html(

                                array("_sft_tipo"), 

                                $args2

                            );
                        };


                        echo '</h1>';

                

                else:

                        echo "<h1>aa</h1>";


                endif;
    */
                ?>











			
			<?php
			if ( have_posts() ) :
                $a = 0;
				echo '<ul class="imoveis-list" id="lista-main">';
				/* Start the Loop */
				while ( have_posts() ) :
                    $a ++;
					the_post();

                	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );


                    echo '<li class="up'.$a.' ss single-item">';


                          if ($a == 3) {
                            $a = 0;
                          } 


                    $terms = get_the_terms( $post->ID , 'tipo' );
                    echo  '<a href="'.get_the_permalink().'" title="'.get_the_title().'"><div  class="img"><div  class="im" style="background-image:url('.$image[0].');" ></div>';
                        foreach ( $terms as $term ) {
                            echo  '<span class="tipo">'.$term->name.'</span>';
                        }
                        echo  '</div>
                        <div class="info">
                            <h4>'.get_the_title().' '.get_field('cod').'</h4>
                            <ul class="infos">';
                                $estados = get_the_terms( $post->ID , 'estado' );
                                if($estados){
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/estado.svg">
                                        <span> <small>Estado</small>';
                                    foreach ( $estados as $estado ) {
                                        echo  '<strong>'.$estado->name.'</strong>';
                                    }

                                    echo  '</span></li>';
                                }

                                /*$regiaos = get_the_terms( $post->ID , 'regiao' );
                                if($regiaos){
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/regiao.svg">
                                        <span> <small>Região</small>';
                                    foreach ( $regiaos as $regiao ) {
                                        echo  '<strong>'.$regiao->name.'</strong>';
                                    }

                                    echo  '</span></li>';
                                }*/


                                $regiao_post = get_field('regiao_post');
                                if( $regiao_post ): 
                                    $string .= '<li>
                                        <img src="'.get_template_directory_uri().'/images/regiao.svg">
                                        <span> <small>Região</small>';
                                        $string .= '<strong>'.esc_html( $regiao_post->post_title ).'</strong>';

                                    $string .= '</span></li>';
                                endif;

                                $climas = get_the_terms( $post->ID , 'clima' );
                                if($climas){
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/clima.svg">
                                        <span> <small>Clima</small>';
                                    foreach ( $climas as $clima ) {
                                        echo  '<strong>'.$clima->name.'</strong>';
                                    }
                                    echo  '</span></li>';
                                }

                                $topografias = get_the_terms( $post->ID , 'topografia' );
                                if($topografias){
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/topografia.svg">
                                        <span> <small>Topografia</small>';
                                    foreach ( $topografias as $topografia ) {
                                        echo  '<strong>'.$topografia->name.'</strong>';
                                    }
                                    echo  '</span></li>';
                                }

                                if( get_field('hectares') ): 
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/hectar.svg">
                                        <span> <small>Área total</small>';
                                        echo  '<strong>'.get_field('hectares').' Hectares</strong>';
                                    echo  '</span></li>';
                                endif;
                            echo  '</ul>
                            </div>
                        </a>
                    </li>';
					




				endwhile;
				wpbeginner_numeric_posts_nav();

				echo '</ul>';
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>

		</div>
	</div>
</section>

<?php
get_footer();
