<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package fazendas_nordeste
 */

get_header();
?>

<section class="sec-imoveis regiao">
	<div class="container">
        <div class="col-xs-12">

                <?php if( get_field('titulo_regiao', 'option') ): ?>
                    <div class="txt">
                            <?php the_field('titulo_regiao', 'option'); ?>
                        
                    </div>
                <?php endif; ?>

                <div class="filtro-reg">
            <?php echo do_shortcode('[searchandfilter id="190"]'); ?>
                    
                </div>




			
			<?php
			if ( have_posts() ) :
                $a = 0;

				echo '<ul class="regioes-list" id="lista-main">';
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

                    $a ++;

                	$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );


                    echo '<li class="up'.$a.' single-item">';


                          if ($a == 2) {
                            $a = 0;
                          } 

                    $terms = get_the_terms( $post->ID , 'tipo' );
                    echo  '<a href="'.get_the_permalink().'" title="'.get_the_title().'">
                                <div  class="img">
                                    <div  class="im" style="background-image:url('.$image[0].');" ></div>
                                    <h4>'.get_the_title().'</h4>
                                </div>';
                                echo '<div class="info">
                                    <p>'.get_the_excerpt().'</p>
                                </div>
                            </a>
                    </li>';
					




				endwhile;
				wpbeginner_numeric_posts_nav();

				echo '</ul>';
			else :
				get_template_part( 'template-parts/content', 'none' );
			endif;
			?>

		</div>
	</div>
</section>

<?php
get_footer();
