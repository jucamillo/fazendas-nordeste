<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package fazendas_nordeste
 */

get_header();
?>

<section class="erro_bg">
	<section class="bottom">
		<div class="container">
			<div class="col-xs-12 col-lg-4 col-md-5 col-sm-6">
					<?php echo get_field('texto_404', 'option'); ?>
					<a href="<?php echo get_home_url(); ?>" class="btn">
						Voltar para Página Inicial
					</a>

			</div>


		</div>
	</section>
</section>

<?php
get_footer();
