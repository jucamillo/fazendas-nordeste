<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
					echo  '<li>';
                    $terms = get_the_terms( $post->ID , 'tipo' );
                    echo  '<a href="'.get_the_permalink().'" title="'.get_the_title().'"><div  class="img"><div  class="im" style="background-image:url('.$image[0].');" ></div>';
                        foreach ( $terms as $term ) {
                            echo  '<span class="tipo">'.$term->name.'</span>';
                        }
                        echo  '</div>
                        <div class="info">
                            <h4>'.get_the_title().'</h4>
                            <ul class="infos">';
                                $estados = get_the_terms( $post->ID , 'estado' );
                                if($estados){
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/estado.svg">
                                        <span> <small>Estado</small>';
                                    foreach ( $estados as $estado ) {
                                        echo  '<strong>'.$estado->name.'</strong>';
                                    }

                                    echo  '</span></li>';
                                }

                                $regiaos = get_the_terms( $post->ID , 'regiao' );
                                if($regiaos){
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/regiao.svg">
                                        <span> <small>Região</small>';
                                    foreach ( $regiaos as $regiao ) {
                                        echo  '<strong>'.$regiao->name.'</strong>';
                                    }

                                    echo  '</span></li>';
                                }

                                $climas = get_the_terms( $post->ID , 'clima' );
                                if($climas){
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/clima.svg">
                                        <span> <small>Clima</small>';
                                    foreach ( $climas as $clima ) {
                                        echo  '<strong>'.$clima->name.'</strong>';
                                    }
                                    echo  '</span></li>';
                                }

                                $topografias = get_the_terms( $post->ID , 'topografia' );
                                if($topografias){
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/topografia.svg">
                                        <span> <small>Topografia</small>';
                                    foreach ( $topografias as $topografia ) {
                                        echo  '<strong>'.$topografia->name.'</strong>';
                                    }
                                    echo  '</span></li>';
                                }

                                if( get_field('hectares') ): 
                                    echo  '<li>
                                        <img src="'.get_template_directory_uri().'/images/hectar.svg">
                                        <span> <small>Área total</small>';
                                        echo  '<strong>'.get_field('hectares').' Hectares</strong>';
                                    echo  '</span></li>';
                                endif;
                            echo  '</ul>
                            </div>
                        </a>
                    </li>';
					 ?>